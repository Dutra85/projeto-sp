object Ftarefa1: TFtarefa1
  Left = 0
  Top = 0
  Anchors = [akLeft, akTop, akRight, akBottom]
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Tarefa 1'
  ClientHeight = 310
  ClientWidth = 686
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDesktopCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object lbColunas: TLabel
    Left = 8
    Top = 35
    Width = 38
    Height = 13
    Caption = 'Colonas'
  end
  object lbTabelas: TLabel
    Left = 181
    Top = 35
    Width = 37
    Height = 13
    Caption = 'Tabelas'
  end
  object lbCondicoes: TLabel
    Left = 392
    Top = 35
    Width = 49
    Height = 13
    Caption = 'Condi'#231#245'es'
  end
  object lbSqlGerado: TLabel
    Left = 16
    Top = 168
    Width = 57
    Height = 13
    Caption = 'SQL Gerado'
  end
  object MemColunas: TMemo
    Left = 8
    Top = 54
    Width = 161
    Height = 89
    TabOrder = 0
    WordWrap = False
    OnExit = MemColunasExit
  end
  object MemTabelas: TMemo
    Left = 175
    Top = 54
    Width = 188
    Height = 89
    TabOrder = 1
    WordWrap = False
    OnExit = MemTabelasExit
  end
  object MemCondicoes: TMemo
    Left = 383
    Top = 54
    Width = 191
    Height = 89
    TabOrder = 2
    WordWrap = False
    OnExit = MemCondicoesExit
  end
  object MemSqlGerado: TMemo
    Left = 16
    Top = 192
    Width = 585
    Height = 89
    TabOrder = 3
  end
  object btSqlGerado: TButton
    Left = 588
    Top = 80
    Width = 75
    Height = 25
    Caption = 'GeraSQL'
    TabOrder = 4
    OnClick = btSqlGeradoClick
  end
end
