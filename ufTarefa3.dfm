object FTarefa3: TFTarefa3
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Tarefa 3'
  ClientHeight = 278
  ClientWidth = 610
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDesktopCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object FPanelTarefa3: TPanel
    Left = 0
    Top = 0
    Width = 610
    Height = 278
    Align = alClient
    TabOrder = 0
    object lbTotal: TLabel
      Left = 469
      Top = 168
      Width = 40
      Height = 13
      Caption = 'Total R$'
    end
    object lbTotaldiv: TLabel
      Left = 469
      Top = 213
      Width = 81
      Height = 13
      Caption = 'Total divis'#245'es R$'
    end
    object dbSPGrid: TDBGrid
      Left = 1
      Top = 1
      Width = 608
      Height = 160
      Align = alTop
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Idprojeto'
          Width = 167
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NomeProjeto'
          Width = 340
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Visible = True
        end>
    end
    object btTotal: TButton
      Left = 376
      Top = 184
      Width = 75
      Height = 25
      Caption = 'Obter Total'
      TabOrder = 1
      OnClick = btTotalClick
    end
    object edtTotal: TEdit
      Left = 469
      Top = 186
      Width = 121
      Height = 21
      NumbersOnly = True
      TabOrder = 2
    end
    object btTotalDiv: TButton
      Left = 344
      Top = 228
      Width = 107
      Height = 25
      Caption = 'Obter Total Divis'#245'es'
      TabOrder = 3
      OnClick = btTotalDivClick
    end
    object edtTotaldiv: TEdit
      Left = 469
      Top = 232
      Width = 121
      Height = 21
      TabOrder = 4
    end
  end
end
