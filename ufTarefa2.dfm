object FTarefa2: TFTarefa2
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'FTarefa2'
  ClientHeight = 253
  ClientWidth = 537
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDesktopCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object FspPanel: TPanel
    Left = 0
    Top = 0
    Width = 537
    Height = 253
    Align = alClient
    TabOrder = 0
    object Flabelthread1: TLabel
      Left = 16
      Top = 16
      Width = 106
      Height = 13
      Caption = 'Milisegundos Thread 1'
    end
    object Flabelthread2: TLabel
      Left = 16
      Top = 44
      Width = 106
      Height = 13
      Caption = 'Milisegundos Thread 2'
    end
    object edMiliseg: TEdit
      Left = 128
      Top = 12
      Width = 121
      Height = 21
      NumbersOnly = True
      TabOrder = 0
    end
    object btExecutar: TButton
      Left = 264
      Top = 10
      Width = 75
      Height = 25
      Caption = 'Executar'
      TabOrder = 1
      OnClick = btExecutarClick
    end
    object pb1: TProgressBar
      Left = 16
      Top = 136
      Width = 489
      Height = 17
      TabOrder = 2
    end
    object pb2: TProgressBar
      Left = 16
      Top = 176
      Width = 489
      Height = 17
      TabOrder = 3
    end
    object edMiliseg2: TEdit
      Left = 128
      Top = 36
      Width = 121
      Height = 21
      NumbersOnly = True
      TabOrder = 4
    end
  end
end
