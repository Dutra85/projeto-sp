unit ProvaDelphiApp;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, ufTarefa1, ufTarefa2, ufTarefa3,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus;

type
  TFMenuPrincipal = class(TForm)
    MenuPrincipal: TMainMenu;
    Tarefas: TMenuItem;
    Tarefa1: TMenuItem;
    Tarefa2: TMenuItem;
    Tarefa3: TMenuItem;
    procedure Tarefa1Click(Sender: TObject);
    procedure Tarefa2Click(Sender: TObject);
    procedure Tarefa3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FMenuPrincipal: TFMenuPrincipal;

implementation


{$R *.dfm}

procedure TFMenuPrincipal.Tarefa1Click(Sender: TObject);
begin
  TFtarefa1.CriaTelaTarefa1;
end;

procedure TFMenuPrincipal.Tarefa2Click(Sender: TObject);
begin
  TFtarefa2.CriaTelaTarefa2;
end;

procedure TFMenuPrincipal.Tarefa3Click(Sender: TObject);
begin
  TFtarefa3.CriaTelaTarefa3;
end;

end.
