unit ufTarefa3;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls,
  Vcl.Grids, Vcl.DBGrids, Data.DB, Datasnap.DBClient;

type
  TFTarefa3 = class(TForm)
    FPanelTarefa3: TPanel;
    dbSPGrid: TDBGrid;
    btTotal: TButton;
    edtTotal: TEdit;
    lbTotal: TLabel;
    btTotalDiv: TButton;
    edtTotaldiv: TEdit;
    lbTotaldiv: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btTotalClick(Sender: TObject);
    procedure btTotalDivClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FspDataset    : TClientDataSet;
    FspDataSource : TDataSource;
    procedure CriarFieldsDataset;
    procedure VincularComponente;
    procedure PopularInformacoes;
    procedure FomatarDatasetFloat;
    procedure CalcularTotal;
    procedure CalculaDiv;
    function RetornarDivisao(const pnValorA: Currency;const pnValorB: Currency) : Currency;


  public
    { Public declarations }
    class procedure CriaTelaTarefa3;
  end;

var
  FTarefa3: TFTarefa3;

implementation

uses Math;

{$R *.dfm}

procedure TFTarefa3.btTotalClick(Sender: TObject);
begin
  CalcularTotal;
end;

procedure TFTarefa3.btTotalDivClick(Sender: TObject);
begin
  CalculaDiv;
end;

procedure TFTarefa3.CalculaDiv;
var
  dValorPrimario: Double;
  dValorSecundario: Double;
  dvalorTotal: Double;
begin
  dValorPrimario := 0;
  dValorSecundario := 0;
  dvalorTotal := 0;
  FspDataset.DisableControls;
  try
    FspDataset.First;
    while not FspDataset.Eof do
    begin
      dValorPrimario := FspDataset.FieldByName('valor').AsCurrency;
      FspDataset.Next;

      dValorSecundario := ifthen(FspDataset.Eof, 0, FspDataset.FieldByName('valor').AsCurrency);

      dvalorTotal := dvalorTotal + RetornarDivisao(dValorPrimario, dValorSecundario);
    end;
    edtTotaldiv.Text := FormatFloat(',0.00', dvalorTotal);
  finally
    FspDataset.EnableControls;
  end;
end;

procedure TFTarefa3.CalcularTotal;
var
  dvalor : Double;
begin
  FspDataset.DisableControls;
  dvalor := 0;
  try
    FspDataset.First;
    while not FspDataset.Eof do
    begin
      dvalor := dvalor + FspDataset.FieldByName('valor').AsFloat;

      FspDataset.Next;
    end;
    edtTotal.Text := FormatFloat(',0.00', dvalor);
  finally
    FspDataset.EnableControls;
  end;
end;

procedure TFTarefa3.CriarFieldsDataset;
begin
  FspDataset.Close;
  FspDataset.FieldDefs.Clear;
  FspDataset.FieldDefs.Add('idProjeto', ftInteger, 0);
  FspDataset.FieldDefs.Add('NomeProjeto', ftString, 100);
  FspDataset.FieldDefs.Add('Valor', ftFloat, 0);
  FspDataset.CreateDataSet;
end;

class procedure TFTarefa3.CriaTelaTarefa3;
begin
  if (not Assigned(Application.FindComponent(('Ftarefa1')))) then
    Application.CreateForm(TFtarefa3, Ftarefa3);

  Ftarefa3.Show;
end;

procedure TFTarefa3.FomatarDatasetFloat;
Begin
  TFloatField(FspDataset.FieldByName('Valor')).DisplayFormat := 'R$ #,##0.00';
End;

procedure TFTarefa3.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
  FTarefa3 := nil;
end;

procedure TFTarefa3.FormCreate(Sender: TObject);
begin
  FspDataset    := TClientDataSet.Create(Self);
  FspDataSource := TDataSource.Create(Self);
  CriarFieldsDataset;
  FomatarDatasetFloat;
  PopularInformacoes;
  VincularComponente;
end;

procedure TFTarefa3.FormDestroy(Sender: TObject);
begin
  FspDataset.Free;
  FspDataSource.Free;
end;

procedure TFTarefa3.PopularInformacoes;
var
  icontador : Integer;
begin
  FspDataset.DisableControls;
  try
    for icontador := 1 to 10 do
    begin
      FspDataset.Append;
      FspDataset.FieldByName('idprojeto').Value := icontador;
      FspDataset.FieldByName('NomeProjeto').Value := 'Projeto ' + IntToStr(icontador);
      FspDataset.FieldByName('Valor').Value := Random(100);
      FspDataset.Post;
    end;
  finally
    FspDataset.EnableControls;
  end;
end;

function TFTarefa3.RetornarDivisao(const pnValorA,
  pnValorB: Currency): Currency;
begin
  if ((pnValorA = 0) or (pnValorB = 0)) then
    Exit(0);

  Result := pnValorA / pnValorB;
end;

procedure TFTarefa3.VincularComponente;
begin
  FspDataSource.DataSet := FspDataset;
  dbSPGrid.DataSource := FspDataSource;
end;

end.
