object FMenuPrincipal: TFMenuPrincipal
  Left = 0
  Top = 0
  Caption = 'MenuPrincipal'
  ClientHeight = 460
  ClientWidth = 1124
  Color = clBtnFace
  DefaultMonitor = dmDesktop
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIForm
  Menu = MenuPrincipal
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object MenuPrincipal: TMainMenu
    Left = 112
    Top = 56
    object Tarefas: TMenuItem
      Caption = 'Tarefas'
      object Tarefa1: TMenuItem
        Caption = 'Tarafa 1'
        OnClick = Tarefa1Click
      end
      object Tarefa2: TMenuItem
        Caption = 'Tarefa 2'
        OnClick = Tarefa2Click
      end
      object Tarefa3: TMenuItem
        Caption = 'Tarefa 3'
        OnClick = Tarefa3Click
      end
    end
  end
end
