unit ufTarefa1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, uSpQuery, uspComponentesRegistro,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TFtarefa1 = class(TForm)
    MemColunas: TMemo;
    MemTabelas: TMemo;
    MemCondicoes: TMemo;
    MemSqlGerado: TMemo;
    lbColunas: TLabel;
    lbTabelas: TLabel;
    lbCondicoes: TLabel;
    lbSqlGerado: TLabel;
    btSqlGerado: TButton;
    procedure btSqlGeradoClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure MemColunasExit(Sender: TObject);
    procedure MemTabelasExit(Sender: TObject);
    procedure MemCondicoesExit(Sender: TObject);
  private
    { Private declarations }
    procedure Validacoes;
    procedure GerarSql;
    procedure ValidaPalavraChaveSelect;
    procedure ValidaPalavraChaveFrom;
    procedure ValidaPalavraChaveWhere;
  public
    { Public declarations }
    class procedure CriaTelaTarefa1;
  end;

var
  Ftarefa1: TFtarefa1;

implementation

{$R *.dfm}

procedure TFtarefa1.btSqlGeradoClick(Sender: TObject);
begin
  Validacoes;
  GerarSql;
end;

class procedure TFtarefa1.CriaTelaTarefa1;
begin
  if (not Assigned(Application.FindComponent(('Ftarefa1')))) then
    Application.CreateForm(TFtarefa1, Ftarefa1);

  Ftarefa1.Show;
end;

procedure TFtarefa1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
  FTarefa1 := nil;
end;

procedure TFtarefa1.Gerarsql;
var
  qry : TspQuery;
begin
  qry := TspQuery.Create(nil);
  try
    qry.spTabelas  .Text := MemTabelas.Text;
    qry.spColunas  .Text := MemColunas.Text;
    qry.spCondicoes.Text := MemCondicoes.Text;

    qry.GeraSQL;
    MemSqlGerado.Lines.Add(qry.SQL.Text);
  finally
    qry.Free;
  end;
end;

procedure TFtarefa1.MemColunasExit(Sender: TObject);
begin
  ValidaPalavraChaveSelect;
end;

procedure TFtarefa1.MemCondicoesExit(Sender: TObject);
begin
  ValidaPalavraChaveWhere;
end;

procedure TFtarefa1.MemTabelasExit(Sender: TObject);
begin
  ValidaPalavraChaveFrom;
end;

procedure TFtarefa1.validacoes;
begin
  if (MemColunas.Text = '') or (MemTabelas.Text = '') or (MemCondicoes.Text = '') then
  begin
     MessageDlg('Por favor, todos os campos devem ser preenchidos para gera��o do sql! verifique.', mtWarning, [mbOK], 0);
     Abort;
  end;
end;

procedure TFtarefa1.ValidaPalavraChaveSelect;
begin
  if (Pos('SELECT', UpperCase(MemColunas.Text)) > 0) then
  begin
     MessageDlg('Por favor, n�o � necess�rio utilizar a palavra chave ''Select''. Verifique.', mtWarning, [mbOK], 0);
     MemColunas.SetFocus;
  end;
end;

procedure TFtarefa1.ValidaPalavraChaveFrom;
begin
  if (Pos('FROM', UpperCase(MemTabelas.Text)) > 0) then
  begin
     MessageDlg('Por favor, n�o � necess�rio utilizar a palavra chave ''From''. Verifique.', mtWarning, [mbOK], 0);
     MemTabelas.SetFocus;
  end;
end;

procedure TFtarefa1.ValidaPalavraChaveWhere;
begin
  if (Pos('WHERE', UpperCase(MemCondicoes.Text)) > 0) then
  begin
     MessageDlg('Por favor, n�o � necess�rio utilizar a palavra chave ''WHERE''. Verifique.', mtWarning, [mbOK], 0);
     MemCondicoes.SetFocus;
  end;
end;

end.


