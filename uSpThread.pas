unit uSpThread;

interface

uses System.Classes, Vcl.ComCtrls;

type

Threadsp = class(TThread)
  private
   FPausa : Integer;
   FBarraProgress: TProgressBar;
   FContador: Integer;
   FProgressoMaximo : Integer;
   procedure AtualizarProgresso;
   procedure ZerarProgressBar;
  protected
    procedure Execute; override;
  public
    procedure IniciarContador;
    constructor Create(const APausa : Integer; const AprogressoMaximo : Integer; AbarraProgress: TProgressBar);
    property Pausa : Integer read FPausa write FPausa;
    class procedure IniciaProcesso(const APausa : Integer; const AprogressoMaximo : Integer; AbarraProgress: TProgressBar);
  end;

implementation

{ Thread1 }

procedure Threadsp.AtualizarProgresso;
begin
  FBarraProgress.Position := FContador;

end;

constructor Threadsp.Create(const APausa:Integer; const AprogressoMaximo : Integer; AbarraProgress: TProgressBar);
begin
  inherited Create(True);
  FreeOnTerminate := True;
  Priority := TpLower;
  FPausa := APausa;
  FBarraProgress := AbarraProgress;
  FContador := 0;
  FProgressoMaximo := AprogressoMaximo;
end;

procedure Threadsp.Execute;
var
   iContador : Integer;
begin
  inherited;
  Synchronize(ZerarProgressBar);
  try
    for iContador := 1 to 100 do
    begin
      FContador := iContador;
      Synchronize(AtualizarProgresso);
      Self.Sleep(FPausa);
    end;
  finally
    Self.Terminate;
  end;
end;

procedure Threadsp.ZerarProgressBar;
begin
  FBarraProgress.Position := 0;
end;

class procedure Threadsp.IniciaProcesso(const APausa: Integer; const AprogressoMaximo : Integer;
  AbarraProgress: TProgressBar);
var
  spthread : Threadsp;
begin
  spthread := Threadsp.Create(APausa, AprogressoMaximo, AbarraProgress);
  spthread.IniciarContador;
end;

procedure Threadsp.IniciarContador;
begin
  Self.Start;
end;

end.
