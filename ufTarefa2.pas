unit ufTarefa2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,  uSpThread,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.ComCtrls;

type
  TFTarefa2 = class(TForm)
    FspPanel: TPanel;
    edMiliseg: TEdit;
    btExecutar: TButton;
    Flabelthread1: TLabel;
    pb1: TProgressBar;
    pb2: TProgressBar;
    edMiliseg2: TEdit;
    Flabelthread2: TLabel;
    procedure btExecutarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    procedure Validar;
  public
    { Public declarations }
    class procedure CriaTelaTarefa2;
  end;

var
  FTarefa2: TFTarefa2;

implementation

{$R *.dfm}

procedure TFTarefa2.btExecutarClick(Sender: TObject);
begin
  Validar;
  Threadsp.IniciaProcesso(StrToInt(edMiliseg.Text), 100, pb1);
  Threadsp.IniciaProcesso(StrToInt(edMiliseg.Text), 100, pb2);
end;

class procedure TFTarefa2.CriaTelaTarefa2;
begin
  if (not Assigned(Application.FindComponent(('Ftarefa1')))) then
    Application.CreateForm(TFtarefa2, Ftarefa2);

  Ftarefa2.Show;
end;

procedure TFTarefa2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
  FTarefa2 := nil;
end;

procedure TFTarefa2.Validar;
begin
 if (edMiliseg.Text = '') or (not (StrToInt(edMiliseg.Text) > 0)) then
 begin
   MessageDlg('Por favor, informe o tempo em Milisegundos! Esse valor deve ser maior que 0. Verifique.', mtWarning, [mbOK], 0);
   edMiliseg.SetFocus;
   Abort;
 end;

  if (edMiliseg2.Text = '') or (not (StrToInt(edMiliseg2.Text) > 0)) then
 begin
   MessageDlg('Por favor, informe o tempo em Milisegundos! Esse valor deve ser maior que 0. Verifique.', mtWarning, [mbOK], 0);
   edMiliseg2.SetFocus;
   Abort;
 end;


end;

end.
