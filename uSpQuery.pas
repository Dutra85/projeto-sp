unit uSpQuery;

interface

uses
  System.SysUtils, System.Classes,
  FireDAC.Phys.MSSQL, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async,
  FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  FireDAC.UI.Intf,FireDAC.Comp.UI;

type
  TspQuery = class(TFDQuery)

  private
    FspCondicoes,
    FspColunas,
    FspTabelas : TStringList;
    procedure ValidarTabelas;
    procedure SetSpCondicoes(const Value : TStringList);
    procedure SetSpColunas  (const Value : TStringList);
    procedure SetSpTabelas  (const Value : TStringList);
    procedure GerarSQL;
  public
    constructor Create(AOwner: TComponent); override;
    procedure GeraSQL;
    destructor Destroy; override;
  published
    property spCondicoes : TStringList read FspCondicoes write FspCondicoes;
    property spColunas   : TStringList read FspColunas   write FspColunas;
    property spTabelas   : TStringList read FspTabelas   write FspTabelas;

  end;

implementation

uses uSpQuery.Consts;

{ TspQuery }

constructor TspQuery.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FspCondicoes := TStringList.Create;
  FspColunas := TStringList.Create;
  FspTabelas := TStringList.Create;
end;

destructor TspQuery.Destroy;
begin
  FreeAndNil(FspCondicoes);
  FreeAndNil(FspColunas);
  FreeAndNil(FspTabelas);
  inherited;
end;

procedure TspQuery.GerarSQL;
var
  iContador : Integer;
begin
  ValidarTabelas;
  Self.SQL.Text := Format(cSelect, [FspColunas.Text, FspTabelas.Text, FspCondicoes.Text]);
end;

procedure TspQuery.GeraSQL;
begin
  GerarSQL;
end;

procedure TspQuery.SetSpColunas(const Value: TStringList);
begin
  FspColunas := Value;
end;

procedure TspQuery.SetSpCondicoes(const Value: TStringList);
begin
  FspCondicoes := Value;
end;

procedure TspQuery.SetSpTabelas(const Value: TStringList);
begin
  FspTabelas := Value;
end;

procedure TspQuery.ValidarTabelas;
var
  iContador : Integer;
begin
  if (FspTabelas.Count > 1) then
    raise Exception.Create('� permitido informar apenas uma tabela para a gera��o do SQL!');

  if (pos(FspTabelas.Text, ',') > 0)  then
    raise Exception.Create('� permitido informar apenas uma tabela para a gera��o do SQL!');
end;

initialization
 RegisterClass(TspQuery);

end.
