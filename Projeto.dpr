program Projeto;

uses
  Vcl.Forms,
  ProvaDelphiApp in 'ProvaDelphiApp.pas' {FMenuPrincipal},
  ufTarefa2 in 'ufTarefa2.pas' {FTarefa2},
  uSpThread in 'uSpThread.pas',
  ufTarefa3 in 'ufTarefa3.pas' {FTarefa3},
  ufTarefa1 in 'ufTarefa1.pas' {Ftarefa1};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFMenuPrincipal, FMenuPrincipal);
  Application.Run;
end.
